\documentclass[11pt]{article}
%Gummi|061|=)

\usepackage{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{booktabs}

\geometry{
 a4paper,
 left=25mm,
 top=25mm,
 right=25mm,
}

\newtheorem{mydef}{Definition}

\title{\textbf{Discrete subgroups of SU(3) from discrete subgroups of SU(2)}}
\author{Dibya Chakravorty}

\date{}

\begin{document}

\fontsize{13}{12}\selectfont

\maketitle

\section{Motivation}

Lie groups have both continuous and discrete subgroups. The relationships between the continuous subgroups of a Lie group are well known. In particular, there exist universal Dynkin diagram based algorithms to find all continuous subgroups of a semisimple Lie group.

In comparison, not much is known about the embedding of discrete groups in a Lie group. We do not have a general theory of such embeddings. Even though all semisimple Lie groups have already been classified in terms of their finite subgroups, the methods employed to perform these classfications are not universal. The classification schemes can be found in, for example, \cite{grimus}, \cite{hanany} and \cite{greiss}. 

The structure of a Lie algebra $\mathbb{L}$ is completely determined by its constituent SU(2) subalgebras. In fact, this is the major reason why continuous subgroups of $\mathbb{L}$ are easy to find. One simply adds and removes SU(2) subalgebras corresponding to simple roots in order to generate subgroups. One such algorithm is described in \cite{georgi}. 

Given the success of such algortihms, it is reasonable to expect that the discrete subgroups of a Lie group might also be found by similar methods, i.e. by studying the discrete subgroups of the constituent SU(2)s.

In Section \hyperref[sec : con]{3}, we present a conjecture in that spirit. Specifically, we claim that the complete set of discrete subgroups of SU(3) can be derived from the set of discrete subgroups of SU(2). The construction process involves taking the SU(3) superproduct of discrete SU(2) subgroups. The superproduct is described in detail in Section \hyperref[sec : sup]{2}. 

\section{SU(3) superproduct of discrete SU(2) subgroups}
\label{sec : sup}

We denote the two-dimensional defining representation of SU(2) by $SU(2)_{2 \times 2}$ and the three dimensional defining representation of SU(3) by $SU(3)_{3 \times 3}$. Consider an embedding of the finite subgroup $F$ into $SU(2)_{2 \times 2}$,

\begin{equation}
	\phi : F \rightarrow SU(2)_{2 \times 2}.
\end{equation}

The embedding $\phi$ induces two embeddings of $F$ into $SU(3)_{3 \times 3}$.

To see this, consider the two simple roots of SU(3). Each simple root gives rise to a SU(2) subalgebra described by the generators $E_{\alpha}, E_{-\alpha}, \text{and } \alpha \cdot H$. Each such set of generators leaves a distinct two-dimensional subspace of $SU(3)_{3 \times 3}$ invariant. Let us call these subspaces $V_{\alpha_1}$ and $V_{\alpha_2}$. 

Each such subspace induces an embedding of $F$ in $SU(3)_{3 \times 3}$,

\begin{align*}
	\phi_{\alpha_2} : F & \rightarrow SU(3)_{3 \times 3} \\
	\phi_{\alpha_2} : F & \rightarrow SU(3)_{3 \times 3}.
\end{align*}

Here, $\phi_{\alpha_1}$ indicates that the embedding $\phi$ happens over the vector space $V_{\alpha_1 }$, while the leftover 1 dimensional space is left untouched.

As a concrete example, we consider the embedding of the generators of $\widetilde{\mathbb{D}}_2$ (Quaternion group) in $SU(2)_{2 \times 2}$.

\begin{align*}
	\phi : \widetilde{a} & \rightarrow \left( \begin{array}{cc}
					  -i & 0 \\
					  0 & i 
					  \end{array} \right) \\
	\phi : \widetilde{b} & \rightarrow \left( \begin{array}{cc}
					  0 & -1 \\
					  1 & 0 
					  \end{array} \right) \\    
\end{align*}

This induces two embeddings of $\widetilde{\mathbb{D}}_2$ in $SU(3)_{3 \times 3}$. The invariant subspace structure of $SU(3)_{ 3 \times 3}$ is particularly simple. The invariant subspaces corresponding to the two simple roots, $\alpha_1$ and $\alpha_2$ are simply:

\[ V_{\alpha_1} = \left( \begin{array}{c} v \\ w \\ 0 \end{array}\right) \text{and } V_{\alpha_2} = \left( \begin{array}{c} 0 \\ v \\ w \end{array}\right) \].


Therefore, the first embedding, $\phi_{\alpha_1}$, is given by

\begin{align*}
	\phi_{\alpha_1} : \widetilde{a} & \rightarrow \left( \begin{array}{ccc}
					  -i & 0 & 0\\
					  0 & i & 0 \\
					  0 & 0 & 1
					  \end{array} \right) \equiv A_1 \\
	\phi_{\alpha_1} : \widetilde{b} & \rightarrow \left( \begin{array}{ccc}
					  0 & -1 & 0 \\
					  1 & 0 & 0 \\
					  0 & 0 & 1
					  \end{array}  \right) \equiv B_1 .\\  
\end{align*}

The second embedding, $\phi_{\alpha_2}$, is given by

\begin{align*}
	\phi_{\alpha_2} : \widetilde{a} & \rightarrow \left( \begin{array}{ccc}
					  1 & 0 & 0\\
					  0 & -i & 0 \\
					  0 & 0 & i
					  \end{array}  \right) \equiv A_2 \\
	\phi_{\alpha_2} : \widetilde{b} & \rightarrow \left( \begin{array}{ccc}
					  1 & 0 & 0 \\
					  0 & 0 & -1 \\
					  0 & 1 & 0
					  \end{array}  \right) \equiv B_2 .\\  .   
\end{align*}

The superproduct of two SU(2) subgroups is defined as the closure of these two embeddings. In this example, this is the group generated by all four generators

\[  A_1 = \left(\begin{array}{ccc}
					  -i & 0 & 0\\
					  0 & i & 0 \\
					  0 & 0 & 1
					  \end{array} \right), B_1 = \left( \begin{array}{ccc}
					  0 & -1 & 0 \\
					  1 & 0 & 0 \\
					  0 & 0 & 1
					  \end{array} \right), A_2 = \left( \begin{array}{ccc}
					  1 & 0 & 0\\
					  0 & -i & 0 \\
					  0 & 0 & i
					  \end{array} \right), \text{and } B_2 = 
					  \left( \begin{array}{ccc}
					  1 & 0 & 0 \\
					  0 & 0 & -1 \\
					  0 & 1 & 0
					  \end{array} \right).
					\]
 
\pagebreak
 
\begin{mydef}

Consider two finite groups $F_1, F_2 \subset SU(2)$ . We define

\begin{align*}
	G_1 & \equiv \{ \phi_{\alpha_1} (f_1) | f_1 \in F_1 \} \\
	G_2 & \equiv \{ \phi_{\alpha_2} (f_2) | f_2 \in F_2 \}.  
\end{align*}

The SU(3) superproduct of the two finite groups $F_1$ and $F_2$ is given by the group generated by $G_1 \cup G_2$.

\end{mydef}

The order of a superproduct is typically larger than the order of the direct product between the same groups. For example, the superproduct of $\widetilde{\mathbb{D}}_2$ with itself gives rise to to a 96 dimensional group which has the \href{http://www.icm.tu-bs.de/ag_algebra/software/small/}{Small Group} ID (96,64). In contrast, the direct product of $\widetilde{\mathbb{D}}_2$ with itself has order 64.

\section{The conjecture}
\label{sec : con}

We conjecture that every discrete subgroup of SU(3) is isomorphic to a subgroup of the superproduct of two discrete subgroups of SU(2).

\section{A GAP package for calculating superproducts}

To test this conjecture for orders listed in the \href{http://www.icm.tu-bs.de/ag_algebra/software/small/}{Small Group Database}, we have written the GAP package \emph{superproduct}. Installation instructions and a short demo is available in this \href{https://bitbucket.org/DibyaC/superproduct}{Bitbucket repository}.  

\section{Results}

Using the GAP package, we have been able to reproduce a few non trivial and popular finite subgroups of SU(3) from the superproduct of two finite SU(2) subgroup. The results are summarized in the table below:

\begin{table}[h!]
  \centering
  \caption{Finite SU(3) subgroups as subgroups of a superproduct }
  \label{tab:table1}
  \begin{tabular}{ccccc}
    \toprule
    SU(3)  & Relation to & SU(2)  & SU(2) & Order of \\
    subgroup & superproduct & subgroup 1 & subgroup 2 & superproduct \\
    \midrule
    $\Delta(27)$ & $\subset$ & $\mathbb{D}_6$ & $\mathbb{D}_6$ & 54 \\
    $\Delta(54)$ & $\cong$ & $\mathbb{D}_6$ & $\mathbb{D}_6$ & 54 \\
    $T_7$ & $\subset$ & $\mathbb{D}_6$ & $\mathbb{D}_{14}$ & 2646 \\
    \bottomrule
  \end{tabular}
\end{table}

\section{Outlook}

It remains to be seen whether all non trivial discrete subgroups of SU(3) can be derived using superproducts. We wish to scrutinize the conjecture further to the limits of the GAP package. If the conjecture holds up, we will then proceed to prove it analytically. 

\bibliography{superproduct.bib}{}
\bibliographystyle{utphys.bst}

\end{document}
