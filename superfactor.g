# This package provides the SubGroups function

LoadPackage("SONATA");


# This module returns SU(3) embeddings of subgroups of SU(2) as MASTERLIST and also provides
# the global variable INDEXLIST

Read("SU2subgroups.g");

Read("superproduct.g");

SuperFactor := function(G)

    # Given a finite SU(3) subgroup, tries to find the SU(2) superfactors by brute force 
    
    local ID, dim, isSG, index, indextup, index1, index2, index3, index4, embedding1, embedding2, P, SGList, subgroup;

    ID := IdSmallGroup(G);

    dim := ID[1];
    
    isSG := true;
    
    for index in [1..Length(INDEXLIST)] do
        indextup := INDEXLIST[index];
        index1 := indextup[1];
        index2 := indextup[2];
        Print("Currently processing groups of order ", index1, " and ", index2, " ...\n");
        for index3 in [1..Length(MASTERLIST[index1])] do
            for index4 in [1..Length(MASTERLIST[index2])] do
                if index3 = 1 and index4 =1 then
                    P := DirectProduct(CyclicGroup(index1),CyclicGroup(index2));
                else
                    embedding1 := MASTERLIST[index1][index3][1];
                    embedding2 := MASTERLIST[index2][index4][2];
                    P := Closure(embedding1, embedding2);
                fi;    
                if (P <> 0 and RemInt(Order(P),dim) = 0) then
                    SGList := Subgroups(P);
                    for subgroup in SGList do
                        if Order(subgroup) = dim and IdSmallGroup(subgroup) = ID then
                            if Order(subgroup) = Order(P) then
                                isSG := false;
                            fi;    
                            return rec(group1 := SmallGroup(IdSmallGroup(embedding1)),
                                        group2 := SmallGroup(IdSmallGroup(embedding2)),
                                        subgroup := isSG,
                                        );
                        fi;
                    od; 
                    UnloadSmallGroupsData();       
                fi;
            od;            
        od;
    od;
    return 0;  
end;      
