Read("superproduct.g");
Read("SU2subgroups.g");

#Create a record of superproducts of different families upto a specific order

SPList := function(max)

    local splist, families, index1, index2, family1, family2, spfamily, list1, list2, index3, index4, SP;

    splist := rec();
    families := RecNames(SU2subgroups);
    
    for index1 in [1..Length(families)] do
        for index2 in [index1..Length(families)] do
            family1 := families[index1];
            family2 := families[index2];  
            spfamily := JoinStringsWithSeparator([family1,family2],"");
            splist.(spfamily) := [];
            list1 := SU2subgroups.(family1);
            list2 := SU2subgroups.(family2);
            Print("Now processing families ", family1, " and ", family2, "\n");
            for index3 in [1..Length(list1)] do
                for index4 in [1..Length(list2)] do
                    Print(index3, " ", index4,"\n");
                    if Order(list1[index3])*Order(list2[index4]) > max then
                        break;
                    fi;
                    if family1 = family2 and index3<=index4 then
                        if spfamily = "ZZ" then
                            SP := DirectProduct(list1[index3], list2[index4]);
                        else
                            SP := SuperProduct(list1[index3], list2[index4]);
                        fi;
                    elif family1 <> family2 then
                        SP := SuperProduct(list1[index3], list2[index4]);
                    fi;
                    if Order(SP) > max then
                        break;
                    fi; 
                    Add(splist.(spfamily), SP);           
                od;        
            od; 
        od;            
    od;  
    return splist; 
end;             