#GAP code for computing SU(3) superproduct of finite SU(2) subgroups


# The package repsn provides the function IrreducibleAffordingRepresentation
LoadPackage("repsn");

Closure := function(E1,E2)

    # Returns the closure of two embeddings
    
    local P;

    # We do not return SmallGroup because identifying
    # a group in the Small Group database is computationally
    # expensive
    
    P := ClosureGroup(E1, E2);
    return P;
        
end;  

IsSU := function(G)

    # checks whether all the generators of the group are Special and Unitary
    # if yes, returns true; otherwise returns false

    local matrix, adjoint;

    for matrix in AsList(GeneratorsOfGroup(G)) do
        adjoint := ComplexConjugate(TransposedMat(matrix));
        if Determinant(matrix) <> 1 or 
           adjoint*matrix <> IdentityMat(2) or 
           matrix*adjoint <> IdentityMat(2) then
         
           return false;
        fi;     
    od;
    return true;
end;

SU2Embed := function(G)

    local result, chars, OneD, char, char1, char2, gens, gens1, gens2, index, T;

    result := 0;    
    chars := Irr(G);
    
    # test if there is a two dimensional irrep, and if yes whether it affords
    # a Special Unitary representation.
    
    OneD := [];
    
    for char in chars do
        if (AsList(char)[1] = 2) then
            result := Range(IrreducibleAffordingRepresentation(char));
            if IsSU(result) and (Order(result) = Order(G)) then
                return result;
            fi; 
        elif (AsList(char)[1] = 1) then
            Add(OneD, char);       
        fi;    
    od;    
    
    # test if the one dimensional irreps can be combined to form a 
    # reducible two dimensional rep, and whether this rep is Special, Unitary
    # and non trivial    
    
    for char1 in OneD do
        for char2 in OneD do
            gens := [];
            gens1 := AsList(char1);
            gens2 := AsList(char2);
            for index in [1..Length(gens1)] do
                Add(gens, [[gens1[index], 0],[0, gens2[index]]]);
            od;
            T := Group(gens);
            if IsSU(T) and (Order(T) = Order(G)) then
                result := T;
                return result;
            fi;      
        od;
    od;
   
   return result;
       
end;


# the embedding of SU(2) subgroups in SU(3) is done by identifying the 
# invariant subspaces corresponding to the two SU(2) subalgebras (one 
# for each simple root) and then mimmicking the 2 X 2 representation
# in that invariant subspace.

SU3Embed1 := function(G)

    local newgens, gens, matrix;
    
    newgens := [];
    gens := GeneratorsOfGroup(G);
    for matrix in gens do
        Add(newgens, [[matrix[1][1], matrix[1][2],0],
                      [matrix[2][1], matrix[2][2],0],
                      [0,0,1]]);
    od;
    
    return Group(newgens);    
end;   

SU3Embed2 := function(G)

    local newgens, gens, matrix;
    
    newgens := [];
    gens := GeneratorsOfGroup(G);
    for matrix in gens do
        Add(newgens,[[1,0,0], 
                    [0, matrix[1][1], matrix[1][2]], 
                    [0, matrix[2][1], matrix[2][2]]]);
    od;
    
    return Group(newgens);  
    
    
end; 

SU3Embed := function(G)

    # Given a SU(2) subgroup G, return its two embeddings in SU(3)

    local SU2G;
    
    SU2G := SU2Embed(G);
    
    return [SU3Embed1(SU2G), SU3Embed2(SU2G)]; 

end; 

SuperProduct := function(A,B)
    # declare local function variables 
    local SU2A, SU2B,SU3A1,SU3B2,SP, ID;
    
    # find a SU2 embedding for G1 and G2, else return an error message
    # if no such embedding can be found. 
    # currently, the code only tries to find 2 dimensional embeddings
    # Quesion : Must all finite SU(2) subgroups have a 2 dimensional embedding? 
    SU2A := SU2Embed(A);
    SU2B := SU2Embed(B);
    
    if (SU2A = 0) then
        Print("Error : The first group cannot be embedded in SU(2); probably not a SU(2) subgroup");
        return 0;
    elif (SU2B = 0) then
        Print("Error : The second group cannot be embedded in SU(2); probably not a SU(2) subgroup");
        return 0;    
    fi;
    
    # embed group A in the SU(2) corresponding to simple root 1 of SU(3)
    # embed group B in the SU(2) corresponding to simple root 1 of SU(3)
    SU3A1 := SU3Embed1(SU2A);
    SU3B2 := SU3Embed2(SU2B);
    
    # the Closure Group of these two groups is defined as the superproduct of 
    # the two groups
    SP := Closure(SU3A1, SU3B2);
    ID := IdSmallGroup(SP);
    return SmallGroup(ID[1],ID[2]);

end;        