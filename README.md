Superproduct is a [GAP](http://www.gap-system.org/) package for calculating 
SU(3) [superproducts](tex/superproduct.pdf) of finite SU(2) subgroups. It can also compute the 
superfactors of a finite SU(3) subgroup.

# How do I get set up? #

1. Install GAP ([instructions here](http://www.gap-system.org/Download/index.html))  

2. Clone the repo

```bash
$ git clone https://DibyaC@bitbucket.org/DibyaC/superproduct.git
$ cd superproduct
```

# Import the package in GAP #

1. Enter the GAP shell and type the following: 

```gap
gap>Read("import.g");
```

and that's it! You can now compute superproducts by typing ```SuperProduct(G1,G2)```
and superfactors by typing ```SuperFactor(G)```.

# Examples #

Superproduct of Dihedral group of order 6 with itself:

```gap
gap> D6 := DihedralGroup(6);
<pc group of size 6 with 2 generators>
gap> S := SuperProduct(D6,D6);
<pc group of size 54 with 4 generators>
gap> IdSmallGroup(S);
[ 54, 8 ]
```
Superproduct of the Cyclic Group of order 3 with the Quaternion Group of order 8:

```gap
gap> Z3 := CyclicGroup(3);
<pc group of size 3 with 1 generators>
gap> Q8 := QuaternionGroup(8);
<pc group of size 8 with 3 generators>
gap> S := SuperProduct(Z3,Q8);
<pc group of size 72 with 5 generators>
gap> IdSmallGroup(S);
[ 72, 26 ]
```

The arguments of the ```SuperGroup``` function must be a valid GAP Groups. 
The [GAP Group Library](http://www.gap-system.org/Manuals/doc/ref/chap50.html)
contains information on how to initialize many popular groups.

The output of the ```SuperGroup``` function is a Small Group. Its ID in the 
[Small Group Database](http://www.icm.tu-bs.de/ag_algebra/software/small/) can be
found by using ```IdSmallGroup(G)```, as shown in the examples.

[The Group Properties Wiki](http://groupprops.subwiki.org/wiki/Category:Particular_groups)
has lots of useful information on small groups. 

We can also construct the superfactors of a finite SU(3) subgroup using this
package. The following example demonstrates the procedure for the SU(3) 
subgroup Delta(27).

```gap
gap> D27 := SmallGroup(27,3);
<pc group of size 27 with 3 generators>
gap> F := SuperFactor(D27);
Currently processing groups of order 1 and 1 ...
Currently processing groups of order 2 and 1 ...
Currently processing groups of order 2 and 2 ...
Currently processing groups of order 3 and 1 ...
Currently processing groups of order 3 and 2 ...
Currently processing groups of order 4 and 1 ...
Currently processing groups of order 3 and 3 ...
Currently processing groups of order 4 and 2 ...
Currently processing groups of order 5 and 1 ...
Currently processing groups of order 4 and 3 ...
Currently processing groups of order 5 and 2 ...
Currently processing groups of order 6 and 1 ...
Currently processing groups of order 4 and 4 ...
Currently processing groups of order 5 and 3 ...
Currently processing groups of order 7 and 1 ...
Currently processing groups of order 6 and 2 ...
Currently processing groups of order 7 and 2 ...
Currently processing groups of order 5 and 4 ...
Currently processing groups of order 8 and 1 ...
Currently processing groups of order 6 and 3 ...
Currently processing groups of order 5 and 5 ...
Currently processing groups of order 7 and 3 ...
Currently processing groups of order 6 and 4 ...
Currently processing groups of order 9 and 1 ...
Currently processing groups of order 8 and 2 ...
Currently processing groups of order 6 and 5 ...
Currently processing groups of order 8 and 3 ...
Currently processing groups of order 7 and 4 ...
Currently processing groups of order 9 and 2 ...
Currently processing groups of order 10 and 1 ...
Currently processing groups of order 8 and 4 ...
Currently processing groups of order 9 and 3 ...
Currently processing groups of order 7 and 5 ...
Currently processing groups of order 11 and 1 ...
Currently processing groups of order 6 and 6 ...
rec( group1 := <pc group of size 6 with 2 generators>, 
  group2 := <pc group of size 6 with 2 generators>, subgroup := true )
gap> F1 := F.group1;
<pc group of size 6 with 2 generators>
gap> F2 := F.group2;
<pc group of size 6 with 2 generators>
gap> IsSubGroup := F.subgroup;
true
gap> IdSmallGroup(F1);
[ 6, 1 ]
gap> IdSmallGroup(F2);
[ 6, 1 ]
```
The output of the ```SuperFactor``` function is a record. The record has 
three entires. ```SuperFactor.group1``` is the first SU(2) superfactor, which 
turned out to have the Small Group ID [6,1] in this case. Similarly 
```SuperFactor.group2``` is the second SU(2) superfactor, which also happened
to have the Small Group ID [6,1]. Finally, ```SuperFactor.subgroup``` is 
**true** if the given group was obtained as a subgroup of the superproduct. It is 
**false** if the given group is isomorphic to the superproduct.

# Contact #

Send any queries to dibyachakravorty@gmail.com
