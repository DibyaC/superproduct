Read("superproduct.g");


CompareIndexTuple := function(T1,T2)
   if T1[1]+T1[2] < T2[1] + T2[2] then
       return true;
   else
       return false;
   fi;
end;

SortedIndexTupleList := function(L)

    local S, index1, index2;

    S := [];

    for index1 in [1..Length(L)] do
        for index2 in [1..Length(L)] do
            if index1 >= index2 then
                Add(S, [index1,index2]);
            fi;
        od;
    od;
    
    Sort(S, CompareIndexTuple);
    return S;
    
end;    
    

# SU(2) subgroups 
SU2subgroups := rec();

# Cyclic subgroups
SU2subgroups.Z := [];

# Dihedral subgorups
SU2subgroups.D := [];

# Double cover of Dihedral groups AKA Quaternion groups
SU2subgroups.Q := [];

# Double cover of the Tetrahedral group
#SU2subgroups.T := [SmallGroup(24,3)];

# Double cover of the Octahedral group
SU2subgroups.O := [SmallGroup(48,28)];

# Double cover of Icosahedral group
SU2subgroups.I := [SmallGroup(120,5)]; 

# Add the subgroup series to the record

max_order := 50;

for index in [2..max_order] do
    Add(SU2subgroups.Z, CyclicGroup(index));
    if IsEvenInt(index) and index>=6 then
        Add(SU2subgroups.D, DihedralGroup(index));
    fi;    
    if RemInt(index,4) = 0 then
        Add(SU2subgroups.Q, QuaternionGroup(index));
    fi;
od;   

# Construct a list of SU2 subgroups by order

MASTERLIST := [[]];

Print("Initializing, please wait...\n");

for index in [2..120] do
    if index <=50 then
        Print("Storing embedding information for SU(2) subgroups of order", index,"...", "\n");
        if index = 4 then 
            Add(MASTERLIST, [SU3Embed(SU2subgroups.Z[index-1]), 
                             SU3Embed(SU2subgroups.Q[index/4])]);
        elif RemInt(index,4) = 0 then
            Add(MASTERLIST, [SU3Embed(SU2subgroups.Z[index-1]), 
                             SU3Embed(SU2subgroups.D[(index/2)-2]),
                             SU3Embed(SU2subgroups.Q[index/4])]);
        elif IsEvenInt(index) and index>=6 then
            Add(MASTERLIST, [SU3Embed(SU2subgroups.Z[index-1]), 
                             SU3Embed(SU2subgroups.D[(index/2)-2])] );
        else 
            Add(MASTERLIST, [SU3Embed(SU2subgroups.Z[index-1])]);
        fi;
    else
        Add(MASTERLIST, []);
    fi;             
od;

#Add(MASTERLIST[24], SU3Embed(SU2subgroups.T[1]));
Add(MASTERLIST[48], SU3Embed(SU2subgroups.O[1]));
Add(MASTERLIST[120], SU3Embed(SU2subgroups.I[1]));

INDEXLIST := SortedIndexTupleList(MASTERLIST);